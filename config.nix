{
  config,
  pkgs,
  inputs,
  ...
}: {

  imports = [
    ./hardware.nix
    ./modules/boot.nix
    ./modules/power.nix
    ./modules/waybar.nix
    ./modules/hyprland.nix
  ];
 
  #Nix settings
  nix.settings.experimental-features = [ "nix-command" "flakes" ];
  nix.gc.randomizedDelaySec = "10m";
  nix.package = pkgs.nixVersions.nix_2_16;
  nixpkgs.config.allowUnfree = true;
  nix.settings.trusted-public-keys = [ "hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ=" ];
  nix.settings.substituters = [ "https://cache.iog.io" ];    
  nix.gc = {
    automatic = true;
    dates = "weekly";
  };  
  

  #Network configuration
  networking.hostName = "Sap";
  networking.wireless.iwd.enable = true;
  networking.dhcpcd.extraConfig = "noarp";
  networking.dhcpcd.wait = "background";
  i18n.defaultLocale = "en_GB.UTF-8";  

  #Time
  time.timeZone = "Asia/Kolkata";
   
  #openGL and Vaapi
  hardware.opengl = {
    enable = true;
    extraPackages = with pkgs; [ vaapiIntel ];
    driSupport32Bit = true;
  };

  #User configuration
  users.users.hikarinee = {
    isNormalUser = true;
    extraGroups = [ "wheel" "video" "audio" ];
    shell = pkgs.zsh;
  };
  
  #Programs
  programs.zsh.enable = true;
  programs.mtr.enable = true;
  programs.hyprland = {
    package = inputs.hyprland.packages.${pkgs.stdenv.hostPlatform.system}.hyprland;
    enable = true;
  };
  environment.systemPackages = with pkgs; [
    git
    tree
    vim
    unzip
    thermald
    tlp
    auto-cpufreq
 ];
  
  #Misc
  hardware.opentabletdriver.enable = true; 
  hardware.opentabletdriver.daemon.enable = true; 
  zramSwap.enable = true;
  system.stateVersion = "23.05";
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
  };
  services.dbus.implementation = "broker";
  systemd.services.systemd-journal-flush.enable = false;
}
