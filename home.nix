{
  config,
  pkgs,
  lib,
  ...
}: {
  #home settings
  home.username = "hikarinee";
  home.homeDirectory = "/home/hikarinee";
  home.stateVersion = "23.05";

  #programs
  programs.home-manager.enable = true;
  programs.git.userName = "HikariNee";
  programs.git.userEmail = "HikariNee@protonmail.com";

  programs.brave = {
    enable = true;
    commandLineArgs = [
      "--enable-features=VaapiVideoDecoder"
      "--disable-features=UseChromeOSDirectVideoDecoder"
      "--ozone-platform-hint=auto"
      "--process-per-site"
   ];
  };

  programs.zsh = {
    enable = true;
    shellAliases = {
      update = "cd $HOME/Flake && sudo nixos-rebuild switch --upgrade --flake .#Sap";
    };
    enableCompletion = true;
    enableAutosuggestions = true;
    enableSyntaxHighlighting = true;
    history = {
      size = 10000;
      path = "${config.xdg.dataHome}/zsh/history";
    };
  };

  home.packages = with pkgs; [
    osu-lazer-bin  
    noto-fonts
    material-icons
    flameshot
    noto-fonts-cjk
    noto-fonts-emoji
    nurl
    kitty
    neovim
    hyprpaper
    clang
    ripgrep
    mako
    libnotify
    powertop
    statix
    nil
    playerctl
    pamixer
    grim
    wl-clipboard
    slurp
    iwgtk
    rofi-wayland 
    (pkgs.nerdfonts.override { fonts = [ "FantasqueSansMono" ]; } )
  ];
}
