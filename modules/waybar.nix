{
  config,
  pkgs,
  inputs,
  ...
}: {
  
  home-manager.sharedModules = [{
    programs.waybar = {
      enable = true;
      package = inputs.hyprland.packages."x86_64-linux".waybar-hyprland;
      settings = [{
        layer = "top";
        position = "top";
             "wlr/workspaces" = {
	        "format" = "{icon}";
		"on-click" = "activate";
		"format-icons" = {
                   "default" = "";
		   "active" = "";
		};
	      };

	     "battery" = {
	        "full-at" = 90;
		"format" = "{capacity}% {icon}";
		"format-icons" = [
                    ""
		    ""
		    ""
		    ""
		    ""
		];
		"tooltip-format" = "Power Draw = {power}";
	     };

	     "cpu" = {
                "format" = "{usage} 󰻠";


	     };

	     "clock" = {
                "timezone" = "Asia/Kolkata";
                "tooltip-format" = "<tt><small>{calendar}</small></tt>";
              
                "calendar" = {
                    "mode"           = "year";
                    "mode-mon-col"   = 3;
                    "weeks-pos"      = "right";
                    "on-scroll"      = 1;
                    "on-click-right" = "mode";
                    "format" = {
                       "months" =     "<span color='#ffead3'><b>{}</b></span>";
                       "days" =       "<span color='#ecc6d9'><b>{}</b></span>";
                       "weeks" =      "<span color='#99ffdd'><b>W{}</b></span>";
                       "weekdays" =   "<span color='#ffcc66'><b>{}</b></span>";
                       "today" =      "<span color='#ff6699'><b><u>{}</u></b></span>";
                  };
                };
	     };

	     "memory" = {
                "format" = "{percentage} 󰍛";
	     };

	     "mpris" = {
	        "format" = "DEFAULT: {player_icon} {dynamic}";
	        "format-paused" = "DEFAULT: {status_icon} <i>{dynamic}</i>";
	        "player-icons" = {
		  "default" = "▶";
		  "mpv" = "🎵";
	         };
	        "status-icons" = {
		  "paused" = "⏸";
	         };
             };

	    "network" = {
               "format" = "{ifname}";
	       "format-wifi" = "{essid} ({signalStrength}%) ";
	       "format-ethernet" = "{ipaddr}/{cidr}";
	       "on-click" = "iwgtk";
	    };

	    "tray" = {
               "icon-size" = 13;
	       "spacing" = 10;
	    };

	    "wireplumber" = {
              "format" = "{volume}% {icon}";
              "format-muted" = "";
              "on-click" = "pamixer -i 5";
	      "on-click-right" = "pamixer -d 5";
              "format-icons" = ["" "󰕾" ""];
            };
        	
	modules-left = [ "wlr/workspaces" ];
	modules-center = [ "clock" "mpris" ]; 
	modules-right = [ "battery" "wireplumber" "network" "memory" "cpu" "tray" ];
   }];
  };
}];
}
