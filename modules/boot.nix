{
  config,
  pkgs,
  ...
}: {

  #GRUB
  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda";
  boot.loader.grub.splashImage = null;
  boot.loader.grub.useOSProber = true;

  #Kernel and Initrd
  boot.kernelPackages = pkgs.linuxPackages_latest;
  boot.initrd.systemd.enable = true;
  boot.kernelParams = [ 
    "module_blacklist=nouveau" 
    "cgroup_no_v1=all" 
    "systemd.unified_cgroup_hierarchy=yes" 
    "console=tty1" 
    "mitigations=off" 
    "nowatchdog" 
    "tsc=reliable"
    "b43.allhwsupport=1"
    "loglevel=3"
    "quiet"
  ];
  boot.supportedFilesystems = [ "ntfs" ];

}
